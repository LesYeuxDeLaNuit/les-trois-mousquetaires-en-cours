#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    int id;
    float cost; //1 First //2 Gold //3 Diamond //4 Business
    int subType; 
    int seniority;  //en mois
} Customer;

void displayCustomer(Customer customer)
{
    printf("---------------------------\n");
    printf("Clent numero: %d\n",customer.id);
    printf("Cout de l'abonnement : %f\n",customer.cost);
    printf("Anciennete : %d\n", customer.seniority);
    printf("Abonnement : ");
    switch (customer.subType)
    {
    case 1:
        printf("First\n");
        break;
    case 2 :
        printf("Gold");
        break;
    case 3 :
        printf("Diamond\n");
        break;
    case 4 :
        printf("Business\n");
        break;
    default:
        printf("Error");
        break;
    }
    
}

float reduction(Customer customer){
    float prix;
    switch (customer.subType)
    {
    case 1: prix = 10+5-(customer.seniority/12);break;
    case 2: prix = 10+10-(customer.seniority/12);break;
    case 3: prix = 10+15-(customer.seniority/12);break;
    case 4: prix = 10+20-(customer.seniority/12);break;
    default:break;
    }
    return prix;
}


int main()
{
    Customer eric = {125487,10.5,3,15};

    printf("%d",eric.seniority/12);

    return 0;
}