float getShippingCost(int priority, int weight);
float getGreenShippingCost(int weight);
float getPriorityShippingCost(int weight);


float getShippingCost(int priority, int weight)
{
    float cost = 0;

    if(priority == 0)
    {
        cost = getGreenShippingCost(weight);
    }
    else
    {
        cost = getPriorityShippingCost(weight);
    }
    return cost;
}

float getGreenShippingCost(int weight)
{
    float cost;

    if (weight <= 20)
    {
        cost = 0.97;
    }
    if (weight >= 21 && weight <= 100)
    {
        cost = 1.94;
    }
    if (weight >= 101 && weight <= 250)
    {
        cost = 3.88;
    }
    
    return cost;
}

float getPriorityShippingCost(int weight)
{
    float cost;

    if (weight <= 20)
    {
        cost = 0.97;
    }
    if (weight >= 21 && weight <= 100)
    {
        cost = 1.94;
    }
    if (weight >= 101 && weight <= 250)
    {
        cost = 3.88;
    }
    
    return cost;
}