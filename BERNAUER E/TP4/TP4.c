
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

typedef struct{
    int color;
    int value;
}Card;

void DisplayCard(Card playCard);
void DisplayValue(int value);
void DisplayColor(int color);

int main() {
    Card playCard;
    srand(time(NULL));
    
    for (int i = 0; i < 5; i++){
        int CMAX = 4, CMIN = 1, VMAX = 13, VMIN = 1;
        playCard.color = (rand() % (CMAX - CMIN + 1)) + CMIN;
        playCard.value = (rand() % (VMAX - VMIN + 1)) + VMIN;

        DisplayCard(playCard);
        Sleep(10);
    }
    return 0;
}

void DisplayCard(Card playCard){
    DisplayValue(playCard.value);
    printf(" de ");
    DisplayColor(playCard.color);
}

void DisplayValue(int value){
    switch (value){
    case 11:printf("Valet");break;
    case 12:printf("Dame");break;
    case 13:printf("Roi");break;
    case 1:printf("As");break;
    default:printf("%d",value);break;
    }
}

void DisplayColor(int color){
    switch (color){
    case 1:printf("Pique \n");break;
    case 2:printf("Coeur \n");break;
    case 3:printf("Trefle \n");break;
    case 4: printf("Carreau \n");break;
    }
}